#ifndef POKERLOVEAPPLICATION_H
#define POKERLOVEAPPLICATION_H

#include <QtWidgets/QApplication>

class QSettings;

class PokerLoveApplication : public QApplication
{

public:
    PokerLoveApplication(int & argc, char ** argv);
    ~PokerLoveApplication();

    void loadStyleSheet(QString nameqss);


    QString DatabaseFile() const;
    void setDatabaseFile(const QString &DatabaseFile);

    QString DatabaseUser() const;
    void setDatabaseUser(const QString &DatabaseUser);

    QString DatabasePassword() const;
    void setDatabasePassword(const QString &DatabasePassword);

    QString DatabaseServer() const;
    void setDatabaseServer(const QString &DatabaseServer);

    QSettings *Settings() const;


    QString ApplicationDataDirectory() const;
    void setApplicationDataDirectory(const QString &ApplicationDataDirectory);

private:
    QString m_DatabaseFile;
    QString m_DatabaseUser;
    QString m_DatabasePassword;
    QString m_DatabaseServer;
    QSettings * m_Settings;
    QString m_ApplicationDataDirectory;

signals:

public slots:

};

#endif // POKERLOVEAPPLICATION_H
