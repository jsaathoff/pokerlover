#ifndef PLAPPDEFINE_H
#define PLAPPDEFINE_H

/*
Redefine qApp
 */


#include "pokerloveapplication.h"

#if defined(qApp)
#undef qApp
#endif
#define qApp (static_cast<PokerLoveApplication*>(QCoreApplication::instance()))

#endif // PLAPPDEFINE_H
