#include "databaseconnection.h"
#include <QSqlDatabase>

DatabaseConnection::DatabaseConnection(QObject *parent, QString connectionname, int dialect, QString dbfilename, QString dbhost, QString dbuser, QString dbpassword) :
    QObject(parent)
{
    m_Database = new QSqlDatabase(QSqlDatabase::addDatabase("QIBASE", connectionname));

    if(!dbhost.isEmpty())
        setHost(dbhost);
    if(!dbuser.isEmpty())
        setUserName(dbuser);
    if(!dbpassword.isEmpty())
        setUserPassword(dbpassword);
    if(!dbfilename.isEmpty())
        setDatabaseFileName(dbfilename);

    setDialect(dialect);
}

DatabaseConnection::~DatabaseConnection()
{
    delete m_Database;
}


QString DatabaseConnection::ConnectionName() const
{
    return m_ConnectionName;
}

QString DatabaseConnection::Host() const
{
    return m_Host;
}

void DatabaseConnection::setHost(const QString &Host)
{
    m_Host = Host;
}

QString DatabaseConnection::UserPassword() const
{
    return m_UserPassword;
}

void DatabaseConnection::setUserPassword(const QString &UserPassword)
{
    m_UserPassword = UserPassword;
}

int DatabaseConnection::dialect() const
{
    return m_dialect;
}

void DatabaseConnection::setDialect(int dialect)
{
    m_dialect = dialect;
}

QString DatabaseConnection::UserName() const
{
    return m_UserName;
}

void DatabaseConnection::setUserName(const QString &UserName)
{
    m_UserName = UserName;
}


QSqlDatabase *DatabaseConnection::Database() const
{
    return m_Database;
}

bool DatabaseConnection::ConnectDatabase()
{
    m_Database->setHostName(Host());
    m_Database->setPassword(UserPassword());
    m_Database->setUserName(UserName());
    m_Database->setDatabaseName(DatabaseFileName());
    m_Database->setConnectOptions("ISC_DPB_LC_CTYPE=UTF8");

    m_Database->open();
    return m_Database->isOpen();
}


QString DatabaseConnection::DatabaseFileName() const
{
    return m_DatabaseFileName;
}

void DatabaseConnection::setDatabaseFileName(const QString &DatabaseFileName)
{
    m_DatabaseFileName = DatabaseFileName;
}
