#ifndef DATABASECONNECTION_H
#define DATABASECONNECTION_H

#include <QObject>

class QSqlDatabase;

class DatabaseConnection : public QObject
{
    Q_OBJECT
public:
    explicit DatabaseConnection(QObject *parent = 0, QString connectionname = "", int dialect=3, QString dbfilename = "", QString dbhost = "", QString dbuser = "", QString dbpassword = "");
    ~DatabaseConnection();

    QString ConnectionName() const;

    QString UserName() const;
    void setUserName(const QString &UserName);

    QString UserPassword() const;
    void setUserPassword(const QString &UserPassword);

    QString Host() const;
    void setHost(const QString &Host);

    int dialect() const;
    void setDialect(int dialect);

    QSqlDatabase *Database() const;

    bool ConnectDatabase();

    QString DatabaseFileName() const;
    void setDatabaseFileName(const QString &DatabaseFileName);

private:
    QSqlDatabase* m_Database;

    QString m_ConnectionName;

    QString m_UserName;

    QString m_UserPassword;

    QString m_Host;

    QString m_DatabaseFileName;

    int m_dialect;

signals:

public slots:

};

#endif // DATABASECONNECTION_H
