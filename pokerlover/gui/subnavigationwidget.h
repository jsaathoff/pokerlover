#ifndef MAINNAVIGATIONWIDGET_H
#define MAINNAVIGATIONWIDGET_H

#include "ui_subnavigationwidget.h"

class SubNavigationWidget : public QWidget, private Ui::SubNavigationWidget
{
    Q_OBJECT

public:
    explicit SubNavigationWidget(QWidget *parent = 0);

protected:
    void changeEvent(QEvent *e);

    void paintEvent(QPaintEvent * event);

private slots:

};

#endif // MAINNAVIGATIONWIDGET_H
