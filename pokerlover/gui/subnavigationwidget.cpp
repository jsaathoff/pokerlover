#include "subnavigationwidget.h"
#include "mainwidget.h"
#include <QStyleOption>
#include <QPainter>

SubNavigationWidget::SubNavigationWidget(QWidget *parent) :
    QWidget(parent)
{
    setupUi(this);
    setLayout(horizontalLayout);
}

void SubNavigationWidget::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        retranslateUi(this);
        break;
    default:
        break;
    }
}

void SubNavigationWidget::paintEvent(QPaintEvent *event)
{
    QStyleOption opt;
    opt.init(this);
    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
}
