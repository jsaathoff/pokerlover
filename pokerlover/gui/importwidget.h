#ifndef IMPORTWIDGET_H
#define IMPORTWIDGET_H

#include "ui_importwidget.h"

class ImportWidget : public QWidget, private Ui::ImportWidget
{
    Q_OBJECT

public:
    explicit ImportWidget(QWidget *parent = 0);

protected:
    void changeEvent(QEvent *e);
};

#endif // IMPORTWIDGET_H
