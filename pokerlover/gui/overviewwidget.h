#ifndef OVERVIEWWIDGET_H
#define OVERVIEWWIDGET_H

#include "ui_overviewwidget.h"

class OverViewWidget : public QWidget, private Ui::OverViewWidget
{
    Q_OBJECT
    
public:
    explicit OverViewWidget(QWidget *parent = 0);
    
protected:
    void changeEvent(QEvent *e);
};

#endif // OVERVIEWWIDGET_H
