#include "detailswidget.h"

DetailsWidget::DetailsWidget(QWidget *parent) :
    QWidget(parent)
{
    setupUi(this);
    setLayout(verticalLayout);
}

void DetailsWidget::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        retranslateUi(this);
        break;
    default:
        break;
    }
}
