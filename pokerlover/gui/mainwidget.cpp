#include "mainwidget.h"
#include "../plappdefine.h"
#include "detailswidget.h"
#include "importwidget.h"
#include "overviewwidget.h"

#include <QStackedLayout>
#include <QDebug>


MainWidget::MainWidget(QWidget *parent) :
    QWidget(parent)
{
    setupUi(this);
    setLayout(verticalLayout);
    showOverviewWidget();
}

void MainWidget::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        retranslateUi(this);
        break;
    default:
        break;
    }
}

void MainWidget::showOverviewWidget()
{
    stackedWidget->setCurrentWidget(pageOverViewWidget);
}

void MainWidget::showDetailsWidget()
{
    stackedWidget->setCurrentWidget(pageDetailsWidget);
}

void MainWidget::showImportWidget()
{
    stackedWidget->setCurrentWidget(pageImportWidget);
}
