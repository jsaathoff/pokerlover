#include "importwidget.h"

ImportWidget::ImportWidget(QWidget *parent) :
    QWidget(parent)
{
    setupUi(this);
    setLayout(verticalLayout);
}

void ImportWidget::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        retranslateUi(this);
        break;
    default:
        break;
    }
}
