#include "headerwidget.h"
#include "mainwidget.h"
#include "../plappdefine.h"

#include <QStyleOption>
#include <QPainter>


HeaderWidget::HeaderWidget(QWidget *parent) :
    QWidget(parent)
{
    setupUi(this);
    setLayout(horizontalLayout);
}

void HeaderWidget::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        retranslateUi(this);
        break;
    default:
        break;
    }
}

void HeaderWidget::paintEvent(QPaintEvent *event)
{
    QStyleOption opt;
    opt.init(this);
    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
}

void HeaderWidget::on_overviewButton_clicked()
{
    static_cast<MainWidget*>(parentWidget())->showOverviewWidget();
}

void HeaderWidget::on_detailsButton_clicked()
{
    static_cast<MainWidget*>(parentWidget())->showDetailsWidget();
}

void HeaderWidget::on_importButton_clicked()
{
    static_cast<MainWidget*>(parentWidget())->showImportWidget();
}
