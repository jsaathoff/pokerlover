#ifndef DETAILSWIDGET_H
#define DETAILSWIDGET_H

#include "ui_detailswidget.h"

class DetailsWidget : public QWidget, private Ui::DetailsWidget
{
    Q_OBJECT

public:
    explicit DetailsWidget(QWidget *parent = 0);

protected:
    void changeEvent(QEvent *e);
};

#endif // DETAILSWIDGET_H
