#ifndef HEADERWIDGET_H
#define HEADERWIDGET_H

#include "ui_headerwidget.h"

class HeaderWidget : public QWidget, private Ui::HeaderWidget
{
    Q_OBJECT

public:
    explicit HeaderWidget(QWidget *parent = 0);

protected:
    void changeEvent(QEvent *e);

    void paintEvent(QPaintEvent * event);
private slots:
    void on_overviewButton_clicked();
    void on_detailsButton_clicked();
    void on_importButton_clicked();
};

#endif // HEADERWIDGET_H
