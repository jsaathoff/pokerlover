#include "overviewwidget.h"

OverViewWidget::OverViewWidget(QWidget *parent) :
    QWidget(parent)
{
    setupUi(this);
    setLayout(verticalLayout);
}

void OverViewWidget::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        retranslateUi(this);
        break;
    default:
        break;
    }
}
