#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include "ui_mainwidget.h"

class MainWidget : public QWidget, private Ui::MainWidget
{
    Q_OBJECT

public:
    explicit MainWidget(QWidget *parent = 0);

protected:
    void changeEvent(QEvent *e);

public slots:
    void showOverviewWidget();
    void showDetailsWidget();
    void showImportWidget();

};

#endif // MAINWIDGET_H
