#ifndef MAINNAVIGATIONBUTTON_H
#define MAINNAVIGATIONBUTTON_H

#include <QPushButton>

class MainNavigationButton : public QPushButton
{
    Q_OBJECT
public:
    explicit MainNavigationButton(QWidget *parent = 0);

signals:

public slots:

};

#endif // MAINNAVIGATIONBUTTON_H
