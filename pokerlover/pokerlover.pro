#-------------------------------------------------
#
# Project created by QtCreator 2013-04-03T21:29:50
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = pokerlover
TEMPLATE = app
DESTDIR = ../appdir

CONFIG(debug, debug|release) {
     mac: TARGET = $$join(TARGET,,,_debug)
     win32: TARGET = $$join(TARGET,,,d)
 }



SOURCES +=\
	gui/mainwindow.cpp \
    pokerloveapplication.cpp \
    main.cpp \
    gui/mainwidget.cpp \
    gui/importwidget.cpp \
    gui/detailswidget.cpp \
    gui/overviewwidget.cpp \
    gui/mainnavigationbutton.cpp \
    parser/pokertextfileparser.cpp \
    controller/basecontroller.cpp \
    database/databaseconnection.cpp \
    controller/databasecontroller.cpp \
    gui/subnavigationwidget.cpp \
    gui/headerwidget.cpp

HEADERS  += gui/mainwindow.h \
    gui/mainwindow.h \
    pokerloveapplication.h \
    gui/mainwidget.h \
    gui/importwidget.h \
    gui/detailswidget.h \
    gui/overviewwidget.h \
    gui/mainnavigationbutton.h \
    parser/pokertextfileparser.h \
    plappdefine.h \
    controller/basecontroller.h \
    database/databaseconnection.h \
    controller/databasecontroller.h \
    gui/subnavigationwidget.h \
    gui/headerwidget.h

FORMS    += \
    uiforms/mainwindow.ui \
    uiforms/mainwidget.ui \
    uiforms/importwidget.ui \
    uiforms/detailswidget.ui \
    uiforms/overviewwidget.ui \
    uiforms/headerwidget.ui \
    uiforms/subnavigationwidget.ui

RESOURCES += \
    resource.qrc
