#include "gui/mainwindow.h"
#include "pokerloveapplication.h"
#include <QFile>
#include <QSplashScreen>

#if defined(qApp)
#undef qApp
#endif
#define qApp (static_cast<PokerLoveApplication*>(QCoreApplication::instance()))

int main(int argc, char *argv[])
{
    PokerLoveApplication a(argc, argv);
    a.loadStyleSheet("default");
    MainWindow w;
    w.show();

    /*
    QSplashScreen splash(, QPixmap(":/images/res/splash.png"));
    splash.show();

    splash.showMessage("TEST", Qt::AlignBottom | Qt::AlignLeft, Qt::white);
    */

    return a.exec();
}
