#include "pokerloveapplication.h"
#include "gui/mainwindow.h"
#include <QFile>
#include <QSettings>
#include <QFileInfo>
#include <QStandardPaths>

PokerLoveApplication::PokerLoveApplication(int & argc, char ** argv ) : QApplication(argc, argv)
{
    setOrganizationName("FlowBytes Software");
    setApplicationName("PokerLover");
    setApplicationDisplayName("PokerLover");
    setApplicationDataDirectory(QStandardPaths::writableLocation(QStandardPaths::DataLocation).append("/"));
    m_Settings = new QSettings(ApplicationDataDirectory().append(applicationName()).append(".ini"), QSettings::IniFormat, this);
    m_Settings->setValue("database/directory", ApplicationDataDirectory().append("database/"));
    m_Settings->sync();
}

void PokerLoveApplication::loadStyleSheet(QString nameqss)
{
    QString filename = QApplication::applicationDirPath() + "/styles/" + nameqss + "/style.qss";
    QFile file(filename);
    file.open(QFile::ReadOnly);
    QString styleSheet = QString::fromLatin1(file.readAll());
    setStyleSheet(styleSheet);
}


QString PokerLoveApplication::DatabaseFile() const
{
    return m_DatabaseFile;
}

void PokerLoveApplication::setDatabaseFile(const QString &DatabaseFile)
{
    m_DatabaseFile = DatabaseFile;
}

QString PokerLoveApplication::DatabasePassword() const
{
    return m_DatabasePassword;
}

void PokerLoveApplication::setDatabasePassword(const QString &DatabasePassword)
{
    m_DatabasePassword = DatabasePassword;
}

QString PokerLoveApplication::DatabaseUser() const
{
    return m_DatabaseUser;
}

void PokerLoveApplication::setDatabaseUser(const QString &DatabaseUser)
{
    m_DatabaseUser = DatabaseUser;
}


QString PokerLoveApplication::DatabaseServer() const
{
    return m_DatabaseServer;
}

void PokerLoveApplication::setDatabaseServer(const QString &DatabaseServer)
{
    m_DatabaseServer = DatabaseServer;
}


QSettings *PokerLoveApplication::Settings() const
{
    return m_Settings;
}


QString PokerLoveApplication::ApplicationDataDirectory() const
{
    return m_ApplicationDataDirectory;
}

void PokerLoveApplication::setApplicationDataDirectory(const QString &AppDataDirectory)
{
    m_ApplicationDataDirectory = AppDataDirectory;
}

PokerLoveApplication::~PokerLoveApplication()
{
    delete m_Settings;
}
